#include "ice40.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"

#define ANY 0xFF
#define CLEARBYTES 7
/**
 * @Brief Programming SPI pins for ICE40
 */
#define ICE40_SPI_CS_PIN         GPIO_Pin_2
#define ICE40_SPI_CS_GPIO_PORT   GPIOD

#define ICE40_PORT  						 GPIOB
#define ICE40_CRST_PIN           GPIO_Pin_6
#define ICE40_CDONE_PIN          GPIO_Pin_7

#define PORT_CLKS RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOD

#define ICE40_SPI                SPI3
#define ICE40_SPI_CLK            RCC_APB1Periph_SPI3
#define ICE40_SPI_SCK_PIN        GPIO_Pin_3
#define ICE40_SPI_SCK_SOURCE     GPIO_PinSource3

#define ICE40_SPI_MOSI_PIN       GPIO_Pin_5
#define ICE40_SPI_MOSI_SOURCE    GPIO_PinSource5

#define ICE40_SPI_MISO_PIN       GPIO_Pin_4
#define ICE40_SPI_MISO_SOURCE    GPIO_PinSource4


#define WAIT_SPI_BUFFER_EMPTY()	while((ICE40_SPI->SR & SPI_I2S_FLAG_TXE) == (uint16_t)RESET)
#define WAIT_SPI_BUFFER_RECIEVE() while((ICE40_SPI->SR & SPI_I2S_FLAG_RXNE) == (uint16_t)RESET)

#define IS_DONE(STATE) (GPIO_ReadInputDataBit(ICE40_PORT, ICE40_CDONE_PIN)==(STATE))


/******************************************************************************
*	@Brief Initialiase GPIO pins and SPI peripherals used to program the ICE40  *
******************************************************************************/
void initialise_ice40(void) {
	GPIO_InitTypeDef  GPIO_Init_S;
	SPI_InitTypeDef   SPI_Init_S;



	/* Enble GPIO clks */
	RCC_APB2PeriphClockCmd(PORT_CLKS | RCC_APB2Periph_AFIO, ENABLE);
	/* Enable SPI clk */; 
	RCC_APB1PeriphClockCmd(ICE40_SPI_CLK, ENABLE); 
	/* Remap JTAG but keep SWD pins, they overlap with outr SPI and LED ports */
	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_NoJTRST, ENABLE); 
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	/* Configure ICE40 SPI CS pin*/
	GPIO_Init_S.GPIO_Pin = ICE40_SPI_CS_PIN;
	GPIO_Init_S.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init_S.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(ICE40_SPI_CS_GPIO_PORT, &GPIO_Init_S);
	GPIO_SetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
	
	/* Configure ICE40_CRST_PIN pin, used to reset FPGA} */
	GPIO_Init_S.GPIO_Pin = ICE40_CRST_PIN;
	GPIO_Init(ICE40_PORT	, &GPIO_Init_S);
	GPIO_SetBits(ICE40_PORT, ICE40_CRST_PIN);
	
	/* leave ICE40_CDONE_PIN pin as input with pullup, used to signal FPGA status */
	GPIO_Init_S.GPIO_Pin = ICE40_CDONE_PIN;
	GPIO_Init_S.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(ICE40_PORT, &GPIO_Init_S);

	/* Configure ICE40_SPI pins MOSI & SCK*/
	GPIO_Init_S.GPIO_Pin = ICE40_SPI_SCK_PIN | ICE40_SPI_MOSI_PIN;
	GPIO_Init_S.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init_S.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(ICE40_PORT, &GPIO_Init_S);
	/* Configure ICE40 SPI MISO input floating */
	GPIO_Init_S.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init_S.GPIO_Pin = ICE40_SPI_MISO_PIN;
	GPIO_Init(ICE40_PORT, &GPIO_Init_S);

	/* Configure and initialise ICE40_SPI operation */
	SPI_Init_S.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_Init_S.SPI_Mode = SPI_Mode_Master;
	SPI_Init_S.SPI_DataSize = SPI_DataSize_8b;
	SPI_Init_S.SPI_CPOL = SPI_CPOL_High;
	SPI_Init_S.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_Init_S.SPI_NSS = SPI_NSS_Soft;
	SPI_Init_S.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	SPI_Init_S.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_Init_S.SPI_CRCPolynomial = 7;
	SPI_Init(ICE40_SPI, &SPI_Init_S);
	/* Enable ICE40_SPI */
	SPI_Cmd(ICE40_SPI, ENABLE); 
}

int spi_read_write(SPI_TypeDef* SPIx, uint8_t *rbuf, uint8_t *tbuf, int cnt) {
	int i;

	for (i = 0; i < cnt; i++){
		while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET);
		if (tbuf)
			SPI_I2S_SendData(SPIx, *tbuf++);
		else 
			SPI_I2S_SendData(SPIx, ANY);

		while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET);

		if (rbuf) 
			*rbuf++ = SPI_I2S_ReceiveData(SPIx);
		else 
			SPI_I2S_ReceiveData(SPIx);
	}
	
	return i;
}

int spi_write(SPI_TypeDef* SPIx, uint8_t *tbuf, int cnt) {
	int i;

	for (i = 0; i < cnt; i++){
		while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET);
		if (tbuf) 
			SPI_I2S_SendData(SPIx, *tbuf++);
		else 
			SPI_I2S_SendData(SPIx, ANY);
	}
	
	return i;
}

/******************************************************************************
*  @Brief Reset ICE40 and boot into slave mode ready for programing over SPI  *
******************************************************************************/
uint8_t ice40_reset(void) {
	/* Timing variables */
	uint64_t elapsed = 0;
	uint32_t timeout = 100;

	/* Pull ICE40 CRESET low to reset FPGA */
	GPIO_ResetBits(ICE40_PORT, ICE40_CRST_PIN);
	/* Pull ICE40 SPI CS low whilst resetting FPGA */
	GPIO_ResetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
	/* Wait for a millisecond */
	while(TICK_elapsed_ms(elapsed) < 1);
	elapsed = TICK_get_cur();
	// Release ICE40 reset to boot into Slave mode */
	GPIO_SetBits(ICE40_PORT, ICE40_CRST_PIN);
	/* Makesure ICE40 CDONE has been pulleed low by FPGA in response */
	while(timeout && IS_DONE(Bit_SET))
		timeout--;
	/* Make sure CDONE responded before timeout if not return TIMEOUT error */
	if(!timeout) {
		/* No CDONE response, take CS back high, then return error*/
		GPIO_SetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
		return TIMEOUT;
	} else {
		/* Delay 2 millisecs to allow FPGA to reconfigure itself*/
		while(TICK_elapsed_ms(elapsed) < 2);
		elapsed = TICK_get_cur();
		/* We are all go for programming */
		return OK;
	}
}

/******************************************************************************
* @ Brief Program the ICE40 in Slave mode, transfer bitfile over SPI_         *
******************************************************************************/
uint8_t ice40_program(uint8_t *bitimg, uint32_t len){
	/* Timing Variable */
	uint32_t timeout = 100;

	/* Lets Reset the ICE40, If good proceed otherwise return error */
	if(ice40_reset() != OK) {
		GPIO_SetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
		return ICE_ERROR;
	} else {
		/* Light the LED to indicate we are programming */
		GPIO_ResetBits(LEDP, LED);

		/* Write the bitfile over SPI to ICE40 bitfile */
		spi_write(ICE40_SPI,bitimg,len);

	/* Wait for ICE40 CDONE release, keep clocking dummies until it works or timesout */
	while(timeout && IS_DONE(Bit_RESET)) {
		while (SPI_I2S_GetFlagStatus(ICE40_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ICE40_SPI, ANY);
		timeout--;
	}

	/* Send the ICE40 at least 49 clocks using dummy data */
	for(int i = 0; i < 7;i++) {
		while (SPI_I2S_GetFlagStatus(ICE40_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ICE40_SPI, ANY);
	}
	
	/* If we tiemout before getting CDONE, return error */
	if(!timeout) {
		/* Release ICE40 SPI CS*/
		GPIO_SetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
		return TIMEOUT;
	}
	
	/* Turn LED of indicating success */
	GPIO_SetBits(LEDP, LED);
	/* Release ICE40 SPI CS*/
	GPIO_SetBits(ICE40_SPI_CS_GPIO_PORT, ICE40_SPI_CS_PIN);
	/* We are all good */
	return OK;
	}
}

