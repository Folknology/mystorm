#include <stdint.h>
#include "tick.h"
#include "cfg.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "ice40.h"

extern uint8_t _binary_bitmap_bin_start;
extern uint8_t _binary_bitmap_bin_end;

void init()
{
	// Enable GPIOB clock
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	initialise_ice40();

	// Initialise PB11 is DIG6
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = LED;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LEDP, &GPIO_InitStructure);
	

	// Initialise the tick module
	TICK_init(SystemCoreClock/SYS_TICK_FREQUENCY);

		
}

int main()
{
		unsigned int toggle =0;
		uint64_t elapsed = 0;

		uint32_t len = &_binary_bitmap_bin_end - &_binary_bitmap_bin_start;

	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	init();

	if(len){
		if(ice40_program(&_binary_bitmap_bin_start, len) == OK) {
			//We are good
		} else {
			return 0;
		}
	}
	while(1)
	{
		while(TICK_elapsed_ms(elapsed) < 2000);
		elapsed = TICK_get_cur();
		
		toggle++;
		toggle &=((unsigned int)1);

	if (toggle)
		GPIO_WriteBit(LEDP, LED, Bit_RESET);
	else
				
		GPIO_WriteBit(LEDP, LED, Bit_SET);
	}
	return 0;
}
