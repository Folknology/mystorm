/**
 *  tick.c : allows the accurate measurement of elapsed time
 *  
 *  Author: Mark Sabido
 *  
 *  This module works independently of clock frequency or reload value as it uses the intrinsic global timer variables in it's calculations
 *  
 *  Maxima and minima:
 *  Maximum elapsed time: (0xFFFFFFFFFFFFFFFF/168000000)/(3600*7*24*52) = 3491 years
 */
#include <stdint.h>
#include <string.h>
#include "stm32f10x.h"
//#include "bsp.h"
#include "tick.h"
#include "misc.h"
//#include "dbg.h"
#include "cfg.h"

uint32_t	g_systick_high;		// Gets incremented every time the system timer reloads

// This should be static but the c11 compiler complains
/*static*/ TICK_TASK_OBJ *g_tick_tasks[MAX_TICK_TASKS];

///////////////////////////////////////////////////////////////////////////////////
//
// Debug functions
//
#ifdef DEBUG_TICK
/**
 *  do_calcs - debug function to print the register values
 */
void do_calcs(TICK_DATA *start_td, TICK_DATA *end_td)
{
	//uint64_t counts;
	DBG_print(D0, "St\r\n");
	DBG_printf(D0, "\tg_systick_high:%lu\r\n",(unsigned long)start_td->systick_high);
	DBG_printf(D0, "\tLOAD:%lu\r\n",(unsigned long)start_td->load);
	DBG_printf(D0, "\tVAL:%lu\r\n", (unsigned long)start_td->val);
	
	DBG_print(D0, "End\r\n");
	DBG_printf(D0, "\tg_systick_high:%lu\r\n",(unsigned long)end_td->systick_high);
	DBG_printf(D0, "\tLOAD:%lu\r\n",(unsigned long)end_td->load);
	DBG_printf(D0, "\tVAL:%lu\r\n", (unsigned long)end_td->val);
}

/**
 *  TICK_get_cur_ex - records timer information for debugging purposes
 */
uint64_t
TICK_get_cur_ex(TICK_DATA *td)
{
	uint32_t	old_g_systick_high;
	uint64_t	counts;

	do {
		old_g_systick_high = g_systick_high;
		counts = ((uint64_t)g_systick_high) * (SysTick->LOAD);

		// Add on the current count value
		counts += SysTick->LOAD - SysTick->VAL;
		td->val = SysTick->VAL;
		td->load = SysTick->LOAD;
		td->systick_high = g_systick_high;

	} while (old_g_systick_high != g_systick_high);

	return counts;
}

/**
 *  TICK_elapsed_us_ex - records timer information for debugging purposes
 */
uint64_t
TICK_elapsed_us_ex(
	uint64_t	start,
	TICK_DATA	*start_td
) {
	uint64_t end;
	uint64_t elapsed;
	TICK_DATA		end_td;

	end = TICK_get_cur_ex(&end_td);

	if (end < start)
	{
		DBG_printf(D0,"TICK_ERROR\r\n");
		do_calcs(start_td, &end_td);
		while(1); // TODO remove!!!
	}

	elapsed = end-start;
	elapsed /= (SystemCoreClock/1000000);

	return elapsed;
}
#endif // DEBUG_TICK
///////////////////////////////////////////////////////////////////////////////////
//
// External functions
//

/**
 *  TICK_init - initialises the system clock
 *  The system counts at the oscillator clock rate. The clock rate is defined in SystemCoreClock and can be used for calculations
 *  
 *  For countdown timings that are clock independent, use the following:
 *  
 *  reload = Tdesired * SystemCoreClock
 *  
 *  Note that it is a 24-bit timer so the maximum time period is approximately 100ms for a 160MHz clock
 *  
 */
uint8_t
TICK_init(
	uint32_t reload
) {
	if ((reload - 1) > SysTick_LOAD_RELOAD_Msk)
	{
//		DBG_print(D0, "TICK:Invalid reload value. Systick not active\r\n");
		return (1);      /* Reload value impossible */
	}

	NVIC_InitTypeDef ni = {SysTick_IRQn, NVIC_SYS_TICK_PREEMPTION_PRIORITY, NVIC_SYS_TICK_SUB_PRIORITY, ENABLE};
	NVIC_Init(&ni);

	SysTick->LOAD  = reload - 1;									/* set reload register */
	//NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1);	/* set Priority for Systick Interrupt */
	SysTick->VAL   = 0;												/* Load the SysTick Counter Value */
	SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk		|
					SysTick_CTRL_TICKINT_Msk		|
					SysTick_CTRL_ENABLE_Msk;						/* Enable SysTick IRQ and SysTick Timer */
	return 0;														/* Function successful */

}

/**
 *  TICK_get_cur - returns the current counter value, to be used to calculate elapsed time
 */
inline uint64_t
TICK_get_cur(void)
{
	uint32_t	old_g_systick_high;
	uint64_t	counts;

	do {
		old_g_systick_high = g_systick_high;
		counts = ((uint64_t)g_systick_high) * (SysTick->LOAD);

		// Add on the current count value
		counts += SysTick->LOAD - SysTick->VAL;
	} while (old_g_systick_high != g_systick_high);

	return counts;
}

/**
 *  TICK_elapsed_ns - returns the elapsed time in nano seconds
 */
inline uint64_t
TICK_elapsed_ns(
	uint64_t	start
) {
	uint64_t	end;
	uint64_t	elapsed;

	// First calculate the number of reload events
	// We must account for when the clock reloads in an ISR, we make sure it hasn't changed during the process
	end = TICK_get_cur();
#ifdef DEBUG_TICK
	if (end < start)
	{
		DBG_printf(D0,"TICK_ERROR\r\n");
		while(1);
	}
#endif // DEBUG_TICK

	elapsed = end-start;
	elapsed *= 1000;
	elapsed /= (SystemCoreClock/1000000);

	return elapsed;
}

/**
 *  TICK_elapsed_ms - returns the elapsed time micro seconds.
 */
inline uint64_t
TICK_elapsed_us(
	uint64_t	start
) {
	uint64_t end;
	uint64_t elapsed;

	end = TICK_get_cur();
#ifdef DEBUG_TICK
	if (end < start)
	{
		DBG_printf(D0,"TICK_ERROR\r\n");
		while(1);
	}
#endif // DEBUG_TICK
	elapsed = end-start;
	elapsed /= (SystemCoreClock/1000000);

	return elapsed;
}

/**
 *  TICK_timespan_us - calculates the length of time between two tick values
 */
 // TODO inline
uint64_t
TICK_timespan_us(
	uint64_t start,
	uint64_t end
) {
	uint64_t span;

#ifdef DEBUG_TICK
	if (end < start)
	{
		DBG_printf(D0,"TICK_ERROR\r\n");
		return 0;
	}
#endif // DEBUG_TICK
	span = end-start;
	span /= (SystemCoreClock/1000000);
	return span;
}

/**
 *  TICK_elapsed_ms - returns the elapsed time milli seconds
 */
inline uint64_t
TICK_elapsed_ms(
	uint64_t	start
) {
	uint64_t end;
	uint64_t elapsed;

	end = TICK_get_cur();

#ifdef DEBUG_TICK
	if (end < start)
	{
		DBG_printf(D0,"TICK_ERROR\r\n");
		while(1);
	}
#endif // DEBUG_TICK
	elapsed = end-start;
	elapsed /= (SystemCoreClock/1000);

	return elapsed;
}

/**
 *  TICK_add_task - task will not run until TICK_run_task is called
  * Note that it is not possible to delete a task from the list, only disable it
  * Returns non-zero on success
 */
uint8_t TICK_add_task(TICK_TASK_INIT *init, TICK_TASK_OBJ *obj, volatile uint64_t *start_ticks)
{
	int iter;
	
	for(iter = 0 ; iter < MAX_TICK_TASKS ; iter++)
		if (!g_tick_tasks[iter])
			break;

	if (MAX_TICK_TASKS <= iter)
		return 0;				// No free slots

	obj->it_divisor				= init->it_divisor;
	obj->callback_instance		= init->callback_instance;
	obj->tick_task_cb			= init->tick_task_cb;
	obj->it_divisor_countdown 	= init->it_divisor-1;
	obj->execute				= 0;
	obj->start_ticks			= start_ticks;

	// take a copy of pointer to object
	g_tick_tasks[iter] = obj;

	return 1;
}

/**
 *  TICK_start_task - causes tick to run the specified task. Note the task must already be present in the task list
 */
void TICK_start_task(TICK_TASK_OBJ *task_obj)
{
#ifdef DEBUG_TICK
	DBG_print(D0, "TICK_start_task\r\n");
#endif // DEBUG_TICK
	task_obj->execute = 1;
}

/**
 * TICK_stop_task
 */
void TICK_stop_task(TICK_TASK_OBJ *task_obj)
{
#ifdef DEBUG_TICK
	DBG_print(D0, "TICK_stop_task\r\n");
#endif // DEBUG_TICK
	task_obj->execute = 0;
}


/**
 *  TICK_tasks
 */
void
TICK_tasks(void)
{
	TICK_TASK_OBJ *tto;
	int iter;
	uint64_t cur_ticks;

	cur_ticks = TICK_get_cur();
	for (iter = 0 ; iter < MAX_TICK_TASKS ; iter++)
	{
		tto = g_tick_tasks[iter];
		if (!tto)
			continue;

		if (!tto->execute)
			continue;

		if (0 == tto->it_divisor_countdown)
			tto->it_divisor_countdown = tto->it_divisor-1;
		else
			tto->it_divisor_countdown--;

		if (tto->it_divisor_countdown == tto->it_divisor-1)
		{
			uint64_t elapsed_us;
			elapsed_us = TICK_timespan_us(*(tto->start_ticks), cur_ticks);
			tto->tick_task_cb(tto->callback_instance, &elapsed_us);
		}
	}
}

/**
 * TICK_set_it_divisor
 */
void TICK_set_it_divisor(TICK_TASK_OBJ *task_obj, uint32_t it_divisor)
{
	task_obj->it_divisor = it_divisor;
}
